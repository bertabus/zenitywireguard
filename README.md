Zenity Wireguard simple gui

install with `./install` from regular user accounts

deps
```sh
sudo apt install wireguard resolvconf qrencode
```

edit the file in ~/.zenityWG to include the names of each of your conf files.

To easily create configs for andriod/ios, use the following
```sh
sudo apt install qrencode
cd /etc/wireguard
sudo qrencode -o myConfig.png -t png < myConfig.conf
```