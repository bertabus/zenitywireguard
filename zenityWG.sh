#!/bin/bash
# zenity --list --column Selection --column Profile FALSE bertserv51820 TRUE  -radiolist

# this part sources zenityWG.properyt and makes the first file the default
source $HOME/.zenityWG
wgConfs=""
for i in ${!confArray[@]}; do
	if [ "$i" -eq "0" ]; then
		wgConfs="True ${confArray[$i]}";
	else
		wgConfs="$wgConfs False ${confArray[$i]}";
	fi
done

#Now check if interface is already on our not, if on it asks for px to turn off.
#If wg is off it will ask to select from the list found at zenityWG.property
currentInterface=$(wg |& cut -d' ' -f5) # this pipes error from wg since iface requires sudo
iface="${currentInterface%?}" #this removes the colon at the end of the iface name
	if [ -z "$iface" ]
	then
	ans=$(zenity  --list  --text "Select your profile for wireguard" \
	--radiolist  --column "" --column "Profile" ${wgConfs}); 
		if [ ! -z $ans ]
		then
		echo $ans
		px=$(zenity --password)
		echo -n $px | sudo -S -k wg-quick up $ans
		fi
	else
	px=$(zenity --password --title "Close $iface")
	echo -n $px | sudo -S -k wg-quick down $iface
	fi
unset px
unset iface
unset currentInterface